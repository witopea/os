#!/bin/bash
habilitar_poweroff=1 # 1=sí
habilitar_wallpaper=1 # 1=sí
habilitar_wallpaper_inicio=1 # 1=sí
IES=1 # 1=GEduaandOS, 2=LXQT, 0=Openbox
minutos=10
hora_14="14:30"
hora_20="20:00"

# Este script configura los servicios de usuario para systemd POWEROFF(14:30 y 20:00) Y WALLPAPER(cada x minutos). Está preparado para GEducandOS, LXQT y OpenBox

# Directorio donde se colocarán los archivos de servicios de usuario
systemd_dir=$HOME/.config/systemd/

rm -f $HOME/.xprofile # borro $HOME/.xprofile
rm -Rf $systemd_dir # Limpio los servicios de usuario
cp 2110-waves-dark.png $HOME/.config/2110-waves-dark.png

if [ $IES -eq 1 ]; then
    idioma_geducaandos="LANGUAGE    DEFAULT=es_US:es
LANG    DEFAULT=es_ES.UTF-8
LC_NUMERIC    DEFAULT=es_ES.UTF-8
LC_TIME    DEFAULT=es_ES.UTF-8
LC_MONETARY    DEFAULT=es_ES.UTF-8
LC_PAPER    DEFAULT=es_ES.UTF-8
LC_NAME    DEFAULT=es_ES.UTF-8
LC_ADDRESS    DEFAULT=es_ES.UTF-8
LC_TELEPHONE    DEFAULT=es_ES.UTF-8
LC_MEASUREMENT    DEFAULT=es_ES.UTF-8
LC_IDENTIFICATION    DEFAULT=es_ES.UTF-8
PAPERSIZE    DEFAULT=a4"
    echo "$idioma_geducaandos" > "$HOME/.pam_environment"
fi

change_wallpaper="#!/bin/bash
ies=$IES # 0=Openbox, 1=EducaandOs, 2=Lxqt

# Comprueba el valor de la variable ies
if [ \$ies -eq 1 ]; then
    # GeducaandOS
    comando_wallpaper='gsettings set org.gnome.desktop.background picture-uri \"file://$HOME/.config/2110-waves-dark.png\"'
elif [ \$ies -eq 2 ]; then
    # Lxqt
    comando_wallpaper='/usr/bin/pcmanfm-qt -w /usr/share/ies/wallpapers/2110-waves-dark.png --wallpaper-mode=stretch'
else
    # Openbox
    comando_wallpaper='/usr/bin/feh --bg-fill $HOME/.config/2110-waves-dark.png'
fi

# Ejecutar el comando correspondiente
\$comando_wallpaper
"
if [[ $habilitar_wallpaper -eq 1 || $habilitar_wallpaper_inicio -eq 1 ]]; then
    echo "$change_wallpaper" > $HOME/.config/change-wallpaper.sh
    chmod +x $HOME/.config/change-wallpaper.sh
fi

# HOME/.XPROFILE
if [[ $habilitar_wallpaper_inicio -eq 1 ]]; then
    home_xprofile="$HOME/.config/change-wallpaper.sh &"
    echo $home_xprofile > "$HOME/.xprofile"
    echo "FONDO DE PANTALLA AL INICIAR: El fondo de pantalla se restaurará al iniciar cada sesión."
fi

# SERVICIO WALLPAPER_MINUTES
service_wallpaper="[Unit]
Description=Change wallpaper every $minutos minutes
After=graphical.target

[Service]
Type=simple
ExecStart=$HOME/.config/change-wallpaper.sh
Restart=always
RestartSec=${minutos}m

[Install]
WantedBy=default.target"

# SERVICIO POWEROFF
service_poweroff="[Unit]
Description=Apagar el equipo

[Service]
Type=oneshot
ExecStart=/usr/sbin/poweroff

[Install]
WantedBy=default.target
"

# Crear directorios
if [[ $habilitar_poweroff -eq 1 || $habilitar_wallpaper -eq 1 ]]; then
    mkdir -p $systemd_dir/user/
fi

if [[ $habilitar_wallpaper -eq 1 ]]; then
    # Generamos el servicio wallpaper que se ejecutará cada ${minutos}
    echo "$service_wallpaper" > $HOME/.config/systemd/user/wallpaper.service

    # Habilitar el servicio wallpaper
    systemctl --user enable wallpaper.service > /dev/null 2>&1
    systemctl --user start wallpaper.service &
    echo "SERVICIO WALLPAPER CONFIGURADO: El fondo de pantalla se restaurará cada $minutos minutos."
    sleep 2s
fi

if [[ $habilitar_poweroff -eq 1 ]]; then
    # Generamos el servicio poweroff
    echo "$service_poweroff" > $HOME/.config/systemd/user/poweroff.service
    timer_poweroff_14="[Unit]
Description=Temporizador para apagar el sistema a las ${hora_14}

[Timer]
OnCalendar=*-*-* ${hora_14}:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
    timer_poweroff_20="[Unit]
Description=Temporizador para apagar el sistema a las ${hora_20}

[Timer]
OnCalendar=*-*-* ${hora_20}:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
    # Genero los timer de poweroff
    echo "$timer_poweroff_14" > $HOME/.config/systemd/user/poweroff14.timer
    echo "$timer_poweroff_20" > $HOME/.config/systemd/user/poweroff20.timer
    
    ###########echo "Recargar systemd"
    ###########systemctl --user daemon-reload
    ###########sleep 2s
    
    # NO HABILITAR EL SERVICIO POWEROFF ya que se apaga el equipo justo al iniciar
    
    # Habilito los timer del servicio poweroff
    systemctl --user enable --now poweroff14.timer > /dev/null 2>&1
    systemctl --user enable --now poweroff20.timer > /dev/null 2>&1
    sleep 2s
    echo "SERVICIO POWEROFF CONFIGURADO: El equipo se apagará a las ${hora_14} y ${hora_20}"
fi

#echo "Hago un restart del servicio wallpaper"
#systemctl --user restart wallpaper-minutes.service

if [[ $habilitar_wallpaper -eq 1 || $habilitar_wallpaper_inicio -eq 1 ]]; then
    # Cambiamos el salvapantallas
    $HOME/.config/change-wallpaper.sh
fi

