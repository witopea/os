#!/bin/bash
minutos=5
hora_14="14:30"
hora_20="20:00"

# Este script configura systemd para ejecutar un script que cambia el fondo de pantalla cada X minutos.

# Directorio donde se colocarán los archivos de servicio
systemd_dir=$HOME/.config/systemd/

rm -Rf $systemd_dir # Limpio los servicios de usuario
cp 2110-waves-dark.png $HOME/.config/2110-waves-dark.png

change_wallpaper='#!/bin/bash
# Cambio de fondo de escritorio
/usr/bin/feh --bg-fill $HOME/.config/2110-waves-dark.png
'
echo "$change_wallpaper" > $HOME/.config/change-wallpaper.sh
chmod +x $HOME/.config/change-wallpaper.sh

# HOME/.XPROFILE
home_xprofile="$HOME/.config/change-wallpaper.sh &"
echo "$home_xprofile" > "$HOME/.xprofile"

# SERVICIO WALLPAPER_MINUTES
service_wallpaper="[Unit]
Description=Change wallpaper every $minutos minutes
After=graphical.target

[Service]
Type=simple
ExecStart=$HOME/.config/change-wallpaper.sh
Restart=always
RestartSec=${minutos}m

[Install]
WantedBy=default.target"

# SERVICIO POWEROFF
service_poweroff="[Unit]
Description=Apagar el equipo

[Service]
Type=oneshot
ExecStart=/usr/bin/poweroff

[Install]
WantedBy=default.target
"

# Crear directorios
mkdir -p "$systemd_dir/user/"

echo "Generamos el servicio wallpaper que se ejecutará cada ${minutos}"
echo "$service_wallpaper" > $HOME/.config/systemd/user/wallpaper.service

echo "Guardar el contenido del servicio poweroff en un archivo"
echo "$service_poweroff" > $HOME/.config/systemd/user/poweroff.service
timer_poweroff_14="[Unit]
Description=Temporizador para apagar el sistema a las ${hora_14}

[Timer]
OnCalendar=*-*-* ${hora_14}:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
timer_poweroff_20="[Unit]
Description=Temporizador para apagar el sistema a las ${hora_20}

[Timer]
OnCalendar=*-*-* ${hora_20}:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
echo "Genero los timer de poweroff"
echo "$timer_poweroff_14" > $HOME/.config/systemd/user/poweroff14.timer
echo "$timer_poweroff_20" > $HOME/.config/systemd/user/poweroff20.timer

echo "Recargar systemd"
systemctl --user daemon-reload
sleep 2s

echo "Habilitar el servicio wallpaper"
systemctl --user enable wallpaper.service
systemctl --user start wallpaper.service &
sleep 2s

echo "NO HABILITAR EL SERVICIO POWEROFF ya que se apaga el equipo justo al iniciar"

echo "Habilito los timer del servicio poweroff"
systemctl --user enable --now poweroff14.timer
systemctl --user enable --now poweroff20.timer
sleep 2s

#echo "Hago un restart del servicio wallpaper"
#systemctl --user restart wallpaper-minutes.service

echo "Configuración completada. El script se ejecutará cada $minutos minutos para cambiar el fondo de pantalla. También el equipo se apagará a las ${hora_14} y ${hora_20}"
