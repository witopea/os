#!/bin/bash
cp 2110-waves-dark.png /usr/share/ies/wallpapers/2110-waves-dark.png

# Este script configura systemd para ejecutar un script que cambia el fondo de pantalla cada 15 minutos.

# Directorio donde se colocarán los archivos de servicio y temporizador
systemd_dir="$HOME/.config/systemd/user"

# Contenido del archivo de servicio
service_content="[Unit]
Description=Script para cambiar el fondo de pantalla cada 5 minutos

[Service]
Type=simple
ExecStart=/usr/bin/pcmanfm-qt -w /usr/share/ies/wallpapers/2110-waves-dark.png --wallpaper-mode=stretch"

# Contenido del archivo de temporizador
timer_content="[Unit]
Description=Timer para ejecutar wallpaper.service cada 5 minutos

[Timer]
OnUnitActiveSec=5m
Unit=wallpaper.service

[Install]
WantedBy=timers.target"

# Crear directorio si no existe
mkdir -p "$systemd_dir"

# Guardar el contenido del servicio en un archivo
echo "$service_content" > "$systemd_dir/wallpaper.service"

# Guardar el contenido del temporizador en un archivo
echo "$timer_content" > "$systemd_dir/wallpaper.timer"

# Recargar systemd
systemctl --user daemon-reload

# Habilitar y comenzar el temporizador
systemctl --user enable wallpaper.timer
systemctl --user start wallpaper.timer

# Hago un restart del servicio
systemctl --user restart wallpaper

echo "Configuración completada. El script se ejecutará cada 5 minutos para cambiar el fondo de pantalla."
