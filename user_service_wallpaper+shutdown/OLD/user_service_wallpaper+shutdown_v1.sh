#!/bin/bash
minutos=1

# Este script configura systemd para ejecutar un script que cambia el fondo de pantalla cada 15 minutos.

# Directorio donde se colocarán los archivos de servicio
systemd_dir=$HOME/.config/systemd/

rm -Rf $systemd_dir # Limpio los servicios de usuario
cp 2110-waves-dark.png $HOME/.config/2110-waves-dark.png
cp change-wallpaper.sh $HOME/.config/change-wallpaper.sh

# Contenido del archivo de servicio
#After=network.target
service_content="[Unit]
Description=Change wallpaper every $minutos minutes
After=graphical.target

[Service]
Type=simple
ExecStart=$HOME/.config/change-wallpaper.sh
Restart=always
RestartSec=${minutos}m

[Install]
WantedBy=default.target"

# Contenido del servicio Poweroff
service_poweroff="[Unit]
Description=Apagar el equipo

[Service]
Type=oneshot
ExecStart=/usr/bin/poweroff

[Install]
WantedBy=default.target
"


# Contenido del archivo change-wallpaper.desktop que dejaremos en $HOME/.config/autostart/
wallpaper_desktop="[Desktop Entry]
Type=Application
Exec=$HOME/.config/change-wallpaper.sh
Name=Cambiar Wallpaper
Comment=Cambia el wallpaper al inicio
X-GNOME-Autostart-enabled=true"

# Crear directorios
mkdir -p "$systemd_dir/user/"

#######mkdir -p "$HOME/.config/autostart/"
#######echo "Guardar el contenido de wallpaper.desktop en $HOME/.config/autostart"
#######echo "$wallpaper_desktop" > $HOME/.config/autostart/wallpaper.desktop
#######chmod +x $HOME/.config/autostart/wallpaper.desktop

echo "Guardar el contenido del servicio wallpaper en un archivo"
echo "$service_content" > $HOME/.config/systemd/user/wallpaper.service

echo "Guardar el contenido del servicio poweroff en un archivo"
echo "$service_poweroff" > $HOME/.config/systemd/user/poweroff.service
timer_poweroff_14="[Unit]
Description=Temporizador para apagar el sistema a las 14:30

[Timer]
OnCalendar=*-*-* 09:35:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
timer_poweroff_20="[Unit]
Description=Temporizador para apagar el sistema a las 20:00

[Timer]
OnCalendar=*-*-* 20:00:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
echo "Genero los timer de poweroff"
echo "$timer_poweroff_14" > $HOME/.config/systemd/user/poweroff14.timer
echo "$timer_poweroff_20" > $HOME/.config/systemd/user/poweroff20.timer

echo "Recargar systemd"
systemctl --user daemon-reload

echo "Habilitar el servicio wallpaper"
systemctl --user enable wallpaper.service
systemctl --user start wallpaper.service &

echo "NO HABILITAR EL SERVICIO POWEROFF ya que se apaga el equipo justo al iniciar"
#systemctl --user enable poweroff.service
#systemctl --user start poweroff.service &

echo "Habilito los timer del servicio poweroff"
systemctl --user enable --now poweroff14.timer
systemctl --user enable --now poweroff20.timer


#echo "Hago un restart del servicio wallpaper"
#systemctl --user restart wallpaper.service

echo "Configuración completada. El script se ejecutará cada $minutos minutos para cambiar el fondo de pantalla."
