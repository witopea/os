#!/bin/bash

# Este script permite crear o eliminar el servicio y los temporizadores configurados previamente para apagar el equipo.

# Directorio donde se encuentran los archivos de servicio y temporizador
systemd_dir="$HOME/.config/systemd/user"

# Definir la plantilla para el servicio
service_template="[Unit]
Description=Script para apagar el equipo a las %i

[Service]
Type=oneshot
ExecStart=/usr/bin/shutdown -h now"

# Definir las horas de apagado
horas_apagado=("14:26" "20:00")

# Función para eliminar el servicio y los temporizadores asociados
eliminar_servicio() {
    # Detener y deshabilitar todos los temporizadores asociados a la plantilla
    systemctl --user stop 'shutdown@*.timer' >/dev/null 2>&1
    systemctl --user disable 'shutdown@*.timer' >/dev/null 2>&1

    # Eliminar todos los temporizadores asociados a la plantilla
    find "$systemd_dir" -type f -name "shutdown@*.timer" -exec rm {} + >/dev/null 2>&1

    # Eliminar el archivo de servicio
    rm -f "$systemd_dir/shutdown@.service" >/dev/null 2>&1

    echo "Servicio y temporizadores de apagado eliminados correctamente."
}

# Función para crear el servicio y los temporizadores asociados
crear_servicio() {
    eliminar_servicio # Elimino el servicio antes de crearlo

    # Crear directorio si no existe
    mkdir -p "$systemd_dir"

    # Guardar el contenido de la plantilla del servicio en un archivo
    echo "$service_template" > "$systemd_dir/shutdown@.service"

    # Crear temporizadores para cada hora de apagado
    for hora in "${horas_apagado[@]}"; do
        # Contenido del archivo de temporizador para la hora actual
        timer_content="[Unit]
Description=Timer para ejecutar shutdown@.service a las $hora

[Timer]
OnCalendar=$hora
Persistent=false# Esto evita que el timer se ejecute si enciendes el equipo después de la hora programada

[Install]
WantedBy=timers.target"

        # Nombre único para el temporizador
        timer_name="shutdown@$hora.timer"

        # Guardar el contenido del temporizador en un archivo
        echo "$timer_content" > "$systemd_dir/$timer_name"

        # Habilitar y comenzar el temporizador
        systemctl --user enable "$timer_name"
        systemctl --user start "$timer_name"

    done

    echo "Servicio y temporizadores de apagado creados correctamente."
}

crear_servicio # Elimina servicio y después lo crea.
