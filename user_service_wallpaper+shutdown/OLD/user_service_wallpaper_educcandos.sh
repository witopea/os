#!/bin/bash

# Este script configura systemd para ejecutar un script que cambia el fondo de pantalla cada {$minutos} minutos.

minutos=5

# Ruta del directorio donde se copiará el wallpaper (ajústala según sea necesario)
ruta_destino_wallpaper="$HOME/.config/wallpaper"

# Verificar si la carpeta ~/.wallpaper existe, si no, crearla
if [ ! -d "$ruta_destino_wallpaper" ]; then
    mkdir -p "$ruta_destino_wallpaper"
fi

# Mostrar la ruta del directorio (opcional)
echo "$ruta_destino_wallpaper"

# Ruta de la imagen (ajústala según sea necesario)
nombre_imagen="2110-waves-dark.png"

# Copiar la imagen al directorio de destino
cp "$nombre_imagen" "$ruta_destino_wallpaper"

echo "La imagen se ha copiado correctamente a $ruta_destino_wallpaper"

# Directorio donde se colocarán los archivos de servicio y temporizador
systemd_dir="$HOME/.config/systemd/user"

# Contenido del archivo de servicio
service_content="[Unit]
Description=Script para cambiar el fondo de pantalla cada ${minutos} minutos

[Service]
Type=simple
ExecStart=gsettings set org.gnome.desktop.background picture-uri \"file://$ruta_destino_wallpaper/$nombre_imagen\""
# Contenido del archivo de temporizador
timer_content="[Unit]
Description=Timer para ejecutar wallpaper.service cada ${minutos} minutos

[Timer]
OnUnitActiveSec=${minutos}m
Unit=wallpaper.service

[Install]
WantedBy=timers.target"

# Crear directorio si no existe
mkdir -p "$systemd_dir"

# Guardar el contenido del servicio en un archivo
echo "$service_content" > "$systemd_dir/wallpaper.service"

# Guardar el contenido del temporizador en un archivo
echo "$timer_content" > "$systemd_dir/wallpaper.timer"

# Recargar systemd
systemctl --user daemon-reload

# Habilitar y comenzar el temporizador
systemctl --user enable wallpaper.timer
systemctl --user start wallpaper.timer

# Hago un restart del servicio
systemctl --user restart wallpaper

echo "Configuración completada. El script se ejecutará cada ${minutos} minutos para cambiar el fondo de pantalla."
