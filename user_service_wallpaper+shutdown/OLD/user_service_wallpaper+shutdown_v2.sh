#!/bin/bash
minutos=5

# Este script configura systemd para ejecutar un script que cambia el fondo de pantalla cada 15 minutos.

# Directorio donde se colocarán los archivos de servicio
systemd_dir=$HOME/.config/systemd/

rm -Rf $systemd_dir # Limpio los servicios de usuario
cp 2110-waves-dark.png $HOME/.config/2110-waves-dark.png

change_wallpaper="#!/bin/bash
/usr/bin/feh --bg-fill $HOME/.config/2110-waves-dark.png
"
echo "$change_wallpaper" > $HOME/.config/change-wallpaper.sh
chmod +x $HOME/.config/change-wallpaper.sh
#cp change-wallpaper.sh $HOME/.config/change-wallpaper.sh
#After=network.target
# SERVICIO WALLPAPER_INICIO
service_wallpaper_inicio="[Unit]
Description=Cambia el fondo de pantalla justo al iniciar el sistema
After=graphical.target

[Service]
Type=simple
User=$USER
ExecStart=$HOME/.config/change-wallpaper.sh
Environment=DISPLAY=:0

[Install]
WantedBy=default.target"

# SERVICIO WALLPAPER_MINUTES
service_wallpaper_minutes="[Unit]
Description=Change wallpaper every $minutos minutes
After=graphical.target

[Service]
Type=simple
ExecStart=$HOME/.config/change-wallpaper.sh
Environment=DISPLAY=:0
Restart=always
RestartSec=${minutos}m

[Install]
WantedBy=default.target"

# SERVICIO POWEROFF
service_poweroff="[Unit]
Description=Apagar el equipo

[Service]
Type=oneshot
ExecStart=/usr/bin/poweroff

[Install]
WantedBy=default.target
"

# Contenido del archivo change-wallpaper.desktop que dejaremos en $HOME/.config/autostart/
wallpaper_desktop="[Desktop Entry]
Type=Application
Exec=$HOME/.config/change-wallpaper.sh
Name=Cambiar Wallpaper
Comment=Cambia el wallpaper al inicio
X-GNOME-Autostart-enabled=true"

# Crear directorios
mkdir -p "$systemd_dir/user/"

#######mkdir -p "$HOME/.config/autostart/"
#######echo "Guardar el contenido de wallpaper.desktop en $HOME/.config/autostart"
#######echo "$wallpaper_desktop" > $HOME/.config/autostart/wallpaper.desktop
#######chmod +x $HOME/.config/autostart/wallpaper.desktop

echo "Generamos el servicio wallpaper-inicio"
echo "$service_wallpaper_inicio" > $HOME/.config/systemd/user/wallpaper-inicio.service

echo "Generamos el servicio wallpaper-minutes"
echo "$service_wallpaper_minutes" > $HOME/.config/systemd/user/wallpaper-minutes.service

echo "Guardar el contenido del servicio poweroff en un archivo"
echo "$service_poweroff" > $HOME/.config/systemd/user/poweroff.service
timer_poweroff_14="[Unit]
Description=Temporizador para apagar el sistema a las 14:30

[Timer]
OnCalendar=*-*-* 09:35:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
timer_poweroff_20="[Unit]
Description=Temporizador para apagar el sistema a las 20:00

[Timer]
OnCalendar=*-*-* 20:00:00
Unit=poweroff.service

[Install]
WantedBy=timers.target
"
echo "Genero los timer de poweroff"
echo "$timer_poweroff_14" > $HOME/.config/systemd/user/poweroff14.timer
echo "$timer_poweroff_20" > $HOME/.config/systemd/user/poweroff20.timer

echo "Recargar systemd"
systemctl --user daemon-reload
sleep 2s

echo "Habilitar el servicio wallpaper-minutes"
systemctl --user enable wallpaper-minutes.service
systemctl --user start wallpaper-minutes.service &
sleep 2s

echo "Habilitar el servicio wallpaper-inicio"
systemctl --user enable wallpaper-inicio.service
systemctl --user start wallpaper-inicio.service &
sleep 2s

echo "NO HABILITAR EL SERVICIO POWEROFF ya que se apaga el equipo justo al iniciar"

echo "Habilito los timer del servicio poweroff"
systemctl --user enable --now poweroff14.timer
systemctl --user enable --now poweroff20.timer
sleep 2s

#echo "Hago un restart del servicio wallpaper"
#systemctl --user restart wallpaper-minutes.service

echo "Configuración completada. El script se ejecutará cada $minutos minutos para cambiar el fondo de pantalla."
