#!/bin/bash
#FUENTE: CREO QUE FUE ESTA;
#https://debianfacil.wordpress.com/2011/09/18/openbox-8-cuadro-de-dialogo-para-apagar-el-ordenador/

gxmessage Elige una opción de apagado -center -title "Elige una opción" -font "Sans bold 10" -default "Cancelar" -buttons "_Cancelar":1,"_Cerrar Sesión":2,"_Reiniciar":3,"_Apagar":4 >/dev/null
case $? in
  1)
     echo "Exit";;
  2)
     lxqt-leave --logout;;
  3)
     lxqt-leave --reboot;;
  4)
     lxterminal -e /usr/local/bin/drive APAGAR $0
     ;;
esac