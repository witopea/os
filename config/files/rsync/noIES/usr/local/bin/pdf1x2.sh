#!/bin/bash

# Verificar si pdfjam está instalado
if ! command -v pdfjam &> /dev/null; then
    echo "El programa pdfjam no está instalado."
    echo "Por favor, instálalo antes de continuar."
    exit 1
fi

# Verificar si se proporcionó al menos un argumento
if [ $# -lt 1 ]; then
    echo "Uso: $0 <archivo_pdf> [archivo_secundario_pdf]"
    exit 1
fi

# Nombre del archivo de entrada principal y secundario (opcional)
input_file="$1"
secondary_input_file="$2"

# Nombre del archivo de salida
output_file="output.pdf"

# Si se proporciona un segundo archivo PDF, combinarlo con el primero
if [ ! -z "$secondary_input_file" ]; then
    # Ejecutar pdfjam para combinar las páginas de ambos archivos
    pdfjam --nup 1x2 "$input_file" "$secondary_input_file" --outfile "$output_file" --papersize '{21cm,29.7cm}' --landscape
    echo "Se ha combinado el archivo $input_file con $secondary_input_file y se ha guardado como $output_file."
else
    # Si no se proporciona un segundo archivo PDF, solo combinar el primero
    pdfjam --nup 1x2 "$input_file" --outfile "$output_file" --papersize '{21cm,29.7cm}' --landscape
    echo "Se ha creado el archivo $output_file a partir de $input_file."
fi
