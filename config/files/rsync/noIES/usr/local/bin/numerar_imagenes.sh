#!/bin/bash
':
Poner dentro de la propia imagen el nombre de la imagen. Lo uso para poder seleccionar imagenes a mano de una galería de imágenes.'
# Verifica si ImageMagick está instalado
if ! command -v convert &> /dev/null
then
    echo "Instala ImageMagick en tu sistema."
    exit
fi

# Verifica si se ha proporcionado un directorio
if [ -z "$1" ]
then
    echo "Uso: $0 <directorio_de_imagenes>"
    exit 1
fi

DIRECTORIO=$1
NUEVO_DIRECTORIO="${DIRECTORIO}_numeradas"

# Crea el nuevo directorio si no existe
mkdir -p "$NUEVO_DIRECTORIO"

# Itera sobre todas las imágenes en el directorio
for IMAGEN in "$DIRECTORIO"/*.{jpg,jpeg,png,gif}; do
    if [ -f "$IMAGEN" ]; then
        # Obtiene el nombre del archivo sin la ruta
        NOMBRE_ARCHIVO=$(basename "$IMAGEN")

        # Ruta completa del nuevo archivo
        NUEVA_IMAGEN="$NUEVO_DIRECTORIO/$NOMBRE_ARCHIVO"

        # Añade el nombre de la imagen en la propia imagen y guarda en el nuevo directorio
        convert "$IMAGEN" -gravity South -pointsize 36 -fill white -annotate +0+10 "$NOMBRE_ARCHIVO" "$NUEVA_IMAGEN"
        echo "Nombre añadido y guardado en: $NUEVA_IMAGEN"
    fi
done

echo "Proceso completado. Las imágenes se guardaron en $NUEVO_DIRECTORIO."
