#!/bin/bash

# Este script verifica si una cadena específica existe en un archivo de configuración.
# Si la cadena existe, la elimina del archivo.
# Si la cadena no existe, la agrega al final del archivo.
# Para evitar ejecuciones accidentales (por ejemplo, doble clic), el script requiere
# que se proporcione el parámetro "-y" para confirmar la ejecución.

archivo="$HOME/.config/espanso/match/base.yml"
cadena_a_buscar="#ND3JVcra"

# Verificar si se pasó el parámetro "-y"
if [[ "$1" != "-y" ]]; then
    echo "Este script requiere el parámetro '-y' para ejecutarse."
    echo "Uso: $0 -y"
    exit 1
fi

# Verificar si el archivo existe
if [[ ! -f "$archivo" ]]; then
    echo "El archivo '$archivo' no existe."
    exit 1
fi

# Verificar si la cadena existe en el archivo
if grep -Fxq "$cadena_a_buscar" "$archivo"; then
    # Si la cadena existe, eliminarla
    sed -i "/^$cadena_a_buscar\$/d" "$archivo"
    echo "La cadena se ha eliminado del archivo."
else
    # Si la cadena no existe, agregarla al final del archivo
    echo "$cadena_a_buscar" >> "$archivo"
    echo "La cadena se ha agregado al archivo."
fi