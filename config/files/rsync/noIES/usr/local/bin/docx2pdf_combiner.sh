#!/bin/bash

# Convierte todos los archivos .docx de la carpeta actual a un único archivo PDF
# Utiliza LibreOffice para la conversión y pdfjam para combinar los PDFs
# Crea una carpeta temporal 'tmp' para los archivos PDF intermedios

# Directorio de trabajo
input_dir="."

# Nombre del directorio temporal
tmp_dir="tmp"

# Nombre del archivo PDF de salida
output_file="archivo_resultado.pdf"

# Crear el directorio temporal
mkdir "$tmp_dir"

# Convertir los archivos .docx a PDF en el directorio temporal
libreoffice --headless --convert-to pdf "$input_dir"/*.docx --outdir "$tmp_dir"

# Combinar los archivos PDF en uno solo en la carpeta principal
pdfjam --outfile "$output_file" "$tmp_dir"/*.pdf

# Eliminar el directorio temporal y su contenido
rm -r "$tmp_dir"