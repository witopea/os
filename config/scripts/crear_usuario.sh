#!/bin/bash
motivo="crear usuario básico usuario 1000"
###### SEGURIDAD: PARA QUE NO SE EJECUTE POR ERROR ######
clear
[[ ! $(id -u) = 0 ]] && echo "Salimos porque no eres root" && exit 1
read -sp "Este script es para $motivo . ¿Quieres continuar? (S/N)" seguir
echo ""
[[ ! ${seguir,,} = s ]] && [[ ! ${seguir,,} = y ]] && echo "Salimos porque no quieres continuar" && exit 1

###### ABAJO EMPIEZA REALMENTE EL SCRIPT ######

useradd -u 1000 -s /bin/bash -G adm,cdrom,dip,lpadmin,plugdev,sambashare,sudo -m usuario
