#!/bin/bash

# Definir la ruta del archivo de destino
TARGET_FILE="$HOME/.pam_environment"

# Contenido que se desea copiar al archivo
CONTENT="LANGUAGE    DEFAULT=es_US:es
LANG    DEFAULT=es_ES.UTF-8
LC_NUMERIC    DEFAULT=es_ES.UTF-8
LC_TIME    DEFAULT=es_ES.UTF-8
LC_MONETARY    DEFAULT=es_ES.UTF-8
LC_PAPER    DEFAULT=es_ES.UTF-8
LC_NAME    DEFAULT=es_ES.UTF-8
LC_ADDRESS    DEFAULT=es_ES.UTF-8
LC_TELEPHONE    DEFAULT=es_ES.UTF-8
LC_MEASUREMENT    DEFAULT=es_ES.UTF-8
LC_IDENTIFICATION    DEFAULT=es_ES.UTF-8
PAPERSIZE    DEFAULT=a4"

# Crear o sobrescribir el archivo con el contenido
echo "$CONTENT" > "$TARGET_FILE"

# Confirmar que el archivo ha sido escrito
echo "El archivo $TARGET_FILE ha sido creado/actualizado con el siguiente contenido:"
cat "$TARGET_FILE"
