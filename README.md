# os
Creado originalmente el 31 de abril de 2023.

## Descripción

Configuración de ArchLinux.
Uso este repositorio para configurar una instalación de Arch que suelo implementar después en muchos equipos.

## Scripts
Los script que empiezan por user_ se deben ejecutar como usuario básico.
- El script **user_service_shutdown.sh** lo ejecuto para configurar el apagado del equipo a ciertas horas. Simplemente se ejecuta y te pregunta si quieres crear el servicio o eliminarlo.
- El script **user_service_wallpaper.sh** lo ejecuto para configurar un servicio que cada 15 minutos te pone el fondo de pantalla que he definido por defecto, para que los usuarios no estén cambiandolo constantemente.
- Hay que eliminar cronie
```sudo pacman -Rs cronie```

## Después de una instalación
- Habilita los servicios de usuario de apagado y wallpaper de equipo(cambia el fondo cada 5 minutos)
- Salvapantallas, ponerlo a una hora
- Configura la fuente FreeSans en openbox
- Configura el grub con el icono del instituto y revisa los errores de firmware que salen
- En el IES instala el libro de inglés
- Deshabilitar el borrado del escritorio
- Configurar firefox para que elimine cookies al cerrar y añadir complemento ublock.

## Pendiente de implementar
- Corregir fuente en configuración de openbox y poner tamaño adecuado para que las x de las ventanas cuando se vayan a cerrar se vena mejor.
- Quitar que se eliminen por defecto los archivos del escritorio, pensar en como habilitarlo sólo para sala tic.

## Novedades
- He guardado /etc/cups/printers.conf donde tengo guardadas las dos impresoras que tengo configuradas. Así me ahorro tener que volver a configurarlas tras formateos.

## Authors and acknowledgment
witopea@gmail.com

## License
CC-BY-SA 4.0
